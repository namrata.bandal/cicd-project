package com.cicd.demo.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService{

    @Autowired
    StudentRepo studentRepo;

    @Override
    public void add(Student student) {
        System.out.println("Student"+student);
        studentRepo.save(student);

    }

    @Override
    public Object getAllStudent() {
        return studentRepo.findAll();
    }
}
