package com.cicd.demo.student;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/student")
public class StudentController {
    @Autowired
    StudentService studentService;

    @PostMapping("/add")
    public void studentAdd(@RequestBody Student student){
        studentService.add(student);
    }

    @GetMapping("/allStudent")
    public Object studentAll(){
        return studentService.getAllStudent();
    }
}
